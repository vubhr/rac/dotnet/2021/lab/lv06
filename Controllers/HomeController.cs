﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using RepairShop.Models;

namespace RepairShop.Controllers;

public class HomeController : Controller
{
    private readonly RepairShopDbContext _context;

    public HomeController(RepairShopDbContext context)
    {
        _context = context;
    }

    public IActionResult Index()
    {
        int[] ticketsNumber = new int[3];
        ticketsNumber[0] = _context.Ticket.Count();
        ticketsNumber[1] = _context.Ticket.Where(ticket => ticket.Completed == false).Count();
        ticketsNumber[2] = ticketsNumber[0] - ticketsNumber[1];
        return View(ticketsNumber);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
