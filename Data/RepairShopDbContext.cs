using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RepairShop.Models;

    public class RepairShopDbContext : DbContext
    {
        public RepairShopDbContext (DbContextOptions<RepairShopDbContext> options)
            : base(options)
        {
        }

        public DbSet<RepairShop.Models.Ticket> Ticket { get; set; }
    }
