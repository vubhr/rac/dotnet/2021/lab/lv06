# RepairShop (MVC)

Korištene naredbe:

    dotnet new mvc -o RepairShop --no-https

    dotnet add package Microsoft.EntityFrameworkCore.Design
    dotnet add package Microsoft.EntityFrameworkCore.Sqlite
    dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
    dotnet add package Microsoft.EntityFrameworkCore.SqlServer

    dotnet-aspnet-codegenerator controller -name TicketsController -m Ticket -dc RepairShopDbContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries

    dotnet ef migrations add InitialCreate

    dotnet ef database update